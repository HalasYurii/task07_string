package com.halas;

import com.halas.myView.Menu;

import java.io.File;
import java.util.List;

import static com.halas.fileFunctional.MethodsWithFile.getTextFromFile;
import static com.halas.fileFunctional.sentence.Sentence.getSentence;
import static com.halas.fileFunctional.word.Word.getWords;
import static com.halas.myView.Menu.input;

public class Main {

    public static File fileName = new File(
            "C:\\Java Projects\\task07_String\\ForFastCheck.txt");

    public static File newFileName = new File(
            "C:\\Java Projects\\task07_String\\The Hunger Games.txt");

    public static void changeFile(File fName) {
        System.out.println("Input the location of the new file: ");
        String newName = input.nextLine();

        File fNew = new File(newName);
        if (fNew.isFile()) {
            System.out.println("Fine, we found this file!");
            fileName = fNew;
        } else {
            System.out.println("Error..\nCan't find this file!");
            System.out.println("File: " + newName);
        }

    }

    public static void showSentences(File fName) {
        String text = getTextFromFile(fName);
        getSentence(text).forEach(System.out::println);
    }

    public static void showWords(File fName) {
        String text = getTextFromFile(fName);
        getWords(text).forEach(System.out::println);
    }

    public static void checkCurrentFile(File fName) {
        String everything = getTextFromFile(fName);
        System.out.println("FILE:\n\n " + everything);
    }

    public static int inputInteger() {
        int value;
        while (true) {
            System.out.println("Input length: ");
            String inputInt = input.nextLine();
            try {
                value = Integer.parseInt(inputInt);
                break;
            } catch (Exception e) {
                System.out.println("Error\nPlease try again!\n");
            }
        }
        System.out.println("\n");
        return value;
    }

    public static void showList(List<String> words, String mess) {
        System.out.print(mess);
        words.forEach(a -> System.out.print(a + " "));
        System.out.println();
    }

    public static void main(String[] args) {
        new Menu().show();
    }
}
