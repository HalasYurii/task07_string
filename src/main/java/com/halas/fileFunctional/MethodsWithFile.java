package com.halas.fileFunctional;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.halas.myView.Menu.input;
import static com.halas.fileFunctional.punctuation_mark.PunctuationMark.removeBadSym;

public class MethodsWithFile implements AutoCloseable {

    public static final String startWithVowel = "\\b[eyuioaEYUIOA]+\\w*\\b";


    public static String getTextFromFile(File fileName) {

        try (BufferedReader buffer = new BufferedReader(new FileReader(fileName))) {
            String everything;
            StringBuilder sbAllText = new StringBuilder();
            String line = buffer.readLine();

            while (line != null) {
                sbAllText.append(line);
                line = buffer.readLine();
            }
            everything = sbAllText.toString();
            return removeBadSym(everything);
        } catch (IOException e) {
            System.out.println("Can't open file");
        }
        return "";
    }

    public static String[] removeNullOrEmpty(String[] text) {
        return Arrays.stream(text)
                .filter(value -> value != null && value.length() > 0)
                .toArray(String[]::new);
    }

    public static String getInputtedSymbol() {
        String sym;
        do {
            sym = input.nextLine();
        } while (sym.length() > 1);
        return sym;
    }

    public static List<String> removeNullOrEmpty(List<String> words) {
        return words.stream()
                .filter(value -> value != null && value.length() > 0)
                .collect(Collectors.toList());
    }

    public void close() {
        System.out.println("Close occur");
    }
}
