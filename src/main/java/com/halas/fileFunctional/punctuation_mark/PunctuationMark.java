package com.halas.fileFunctional.punctuation_mark;

public class PunctuationMark {
    public static String removeBadSym(String everything) {
        String patNotOnlyDigit = " *[\\d ]+[.?!]";
        everything = everything.replaceAll(patNotOnlyDigit, "");
        return everything.replaceAll("[ ,\":+\\-=*₴()<>&%@\\]\\[]+", " ").toLowerCase();
    }
}
