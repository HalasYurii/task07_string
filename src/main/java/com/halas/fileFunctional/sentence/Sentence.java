package com.halas.fileFunctional.sentence;


import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.halas.Main.showList;
import static com.halas.fileFunctional.MethodsWithFile.*;
import static com.halas.fileFunctional.punctuation_mark.PunctuationMark.removeBadSym;
import static com.halas.fileFunctional.word.Word.*;

public class Sentence {
    public static List<String> getSentence(String text) {
        String[] sents = text.split(" *[?!.;] *");
        sents = removeNullOrEmpty(sents);
        return Arrays.stream(sents)
                .map(a -> a = a.toLowerCase())
                .collect(Collectors.toList());
    }


    public static List<String> getQuestionSentences(String text) {
        String[] sents = text.split(" *[\\w ]+[!.] *");
        sents = removeNullOrEmpty(sents);
        return new ArrayList<>(Arrays.asList(sents));
    }

    /**
     * речення в яких є однакові слова не виводяться
     */
    public static void showSentencesWithNotSimilarWords(File fName) {
        String text = getTextFromFile(fName);
        //Pattern p1 = Pattern.compile("[^.]+\\.");
        //Matcher m1 = p1.matcher(text);

        //String[] sents = text.split("[?!\\.]");
        List<String> sents = getSentence(text);

        for (String wordW2 : sents) {
            List<String> list = getWords(wordW2);

            //set unique word, then  his count find
            Set<String> st = new HashSet<>(list);
            //System.out.println("Sentence: "+list);
            boolean isSimilarWords = st.stream()
                    .anyMatch(word -> Collections.frequency(list, word) > 1);
            //st.stream().forEach(word-> System.out.println(word+" : "+Collections.frequency(list,word)));
            if (!isSimilarWords) {
                System.out.println(wordW2);
            }
        }
    }

    /**
     * речення в яких є слова з інших речень, task1!
     */
    public static void showSentencesWithSimilarWordsOfSen(File fName) {
        String text = getTextFromFile(fName);
        List<String> sentences = getSentence(text);
        Map<String, Integer> wordAmount = getWordsWithAmount(text);

        Map<String, Integer> forCycle = new HashMap<>(wordAmount);
        for (Map.Entry<String, Integer> val : forCycle.entrySet()) {
            if (val.getValue() == 1) {
                wordAmount.remove(val.getKey(), val.getValue());
            }
        }

        for (String sentence : sentences) {
            for (String value : wordAmount.keySet()) {
                if (sentence.contains(value)) {
                    System.out.println(sentence);
                    break;
                }
            }
        }
        //words with higher than 2 amount
        //System.out.println(map);
    }

    /**
     * речення у порядку зростання к-сті слів у кожному з них
     * сортування речень за довжиною task2
     */
    public static void getSortedByLengthSentences(File fName) {
        String text = getTextFromFile(fName);
        getSentence(text).stream()
                .sorted(Comparator.comparing(String::length))
                .forEach(System.out::println);
    }

    /**
     * поміняти перше слово яке починається на голосну літер
     * з найдовшим словом, для кожного речення task5
     */
    public static void swapFirstVowelAndLongestWords(File fName) {
        String text = getTextFromFile(fName);
        List<String> sentences = getSentence(text);

        Pattern patternVowel = Pattern.compile(startWithVowel);
        for (String sentence : sentences) {
            List<String> words = getWords(sentence);

            Matcher matcher = patternVowel.matcher(sentence);

            //first vowel word
            String vowelWord = "";
            if (matcher.find()) {
                vowelWord = sentence.substring(matcher.start(), matcher.end());
                System.out.println("Vowel word: " + sentence.substring(matcher.start(), matcher.end()));
            }
            if (!vowelWord.isEmpty()) {
                //found vowelWord
                Optional<String> longestOptWord = words
                        .stream()
                        .max(Comparator.comparing(String::length));
                String longestWord;
                if (longestOptWord.isPresent()) {
                    longestWord = longestOptWord.get();
                    System.out.println("Longest word: " + longestWord);
                    int indexMaxWord = words.indexOf(longestWord);
                    int indexVowel = words.indexOf(vowelWord);

                    showList(words, "Before: ");

                    words.set(indexMaxWord, vowelWord);
                    words.set(indexVowel, longestWord);

                    showList(words, "After: ");
                }
            }
        }
    }

    /**
     * У кожному реченні тексту видалити підрядок
     * (як я зроумів - це малось на увазі слово)
     * максимальної довжини, що починається і
     * закінчується заданими символами. task11
     */
    public static void showWithoutMaxWord(File fName) {
        String text = getTextFromFile(fName);
        List<String> sentences = getSentence(text);

        System.out.println("Input symbol symbol of which should start word");
        String firstSymbol = getInputtedSymbol();

        System.out.println("Input symbol symbol of which should end word");
        String lasSymbol = getInputtedSymbol();

        Pattern p = Pattern.compile("\\b" + firstSymbol + "[\\w']+" + lasSymbol + "\\b");
        for (String sentence : sentences) {
            Matcher m = p.matcher(sentence);

            List<String> foundedWords = new LinkedList<>();
            while (m.find()) {
                foundedWords.add(m.group());
            }
            if (!foundedWords.isEmpty()) {
                String max = Collections.max(foundedWords, Comparator.comparing(String::length));
                System.out.println("Before: " + sentence);
                System.out.println("Max: " + max);
                sentence = removeBadSym(sentence.replace(max, ""));
                System.out.println("After: " + sentence + "\n");
            }
        }
    }
}
