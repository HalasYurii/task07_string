package com.halas.fileFunctional.word;


import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.halas.Main.inputInteger;
import static com.halas.myView.Menu.*;
import static com.halas.fileFunctional.MethodsWithFile.*;
import static com.halas.fileFunctional.punctuation_mark.PunctuationMark.removeBadSym;
import static com.halas.fileFunctional.sentence.Sentence.getQuestionSentences;
import static com.halas.fileFunctional.sentence.Sentence.getSentence;

public class Word {

    public static List<String> getWords(String text) {
        text = text.trim();
        //розділити на слова за такими знаками
        String[] words = text.split("[ .!?]+");
        //list  of words
        return Arrays.stream(words)
                .map(a -> a = a.toLowerCase())
                .collect(Collectors.toList());
    }

    public static Map<String, Integer> getWordsWithAmount(String text) {
        List<String> words = getWords(text);

        Map<String, Integer> map = new HashMap<>();
        for (String s : words) {
            Integer i = map.get(s);
            map.put(s, (i == null) ? 1 : i + 1);
        }
        return map;
    }

    /**
     * Unique words, expect that text was processed from punctuation marks
     */
    public static List<String> getUniqueWords(String text) {
        return getWords(text)
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Знайти такі слова у першому реченні, яких немає ні в одному з інших
     * речень. task3
     */
    public static void findWordsThatAreOnlyInFirstSentence(File fName) {
        String text = getTextFromFile(fName);
        String firstSentence = getSentence(text).get(0);
        text = text.replace(firstSentence, "");

        List<String> firstSenWords = getWords(firstSentence);
        Set<String> anotherWords = new HashSet<>(getWords(text));

        firstSenWords.removeAll(anotherWords);
        System.out.println(firstSenWords);
    }

    /**
     * У всіх запитальних реченнях тексту знайти і надрукувати без повторів
     * слова заданої довжини. task4
     */
    public static void findUniqueWordsInTheQuestionnaires(File fName) {
        String text = getTextFromFile(fName);
        List<String> questSen = getQuestionSentences(text);
        System.out.println("Sentences: " + questSen + "\n");
        List<String> words = new ArrayList<>();
        //кладу окремо кожне соло в лист слів
        questSen.forEach(a -> words.addAll(getUniqueWords(a)));
        System.out.println("Words: " + words + "\n");
        int inputLength = inputInteger();
        System.out.println("Unique words with length " + inputLength + ": "
                + words.stream()
                .filter(a -> a.length() == inputLength)
                .collect(Collectors.toList())
        );
    }

    /**
     * Надрукувати слова тексту в алфавітному порядку по першій букві. Слова,
     * що починаються з нової букви, друкувати з абзацного відступу. task6
     */
    public static void showSortedByFirstSymbol(File fName) {
        String text = getTextFromFile(fName);
        List<String> words = getWords(text);

        Collections.sort(words);
        String previousLetter = "";
        for (String word : words) {
            if (!word.startsWith(previousLetter)) {
                System.out.println();
            }
            System.out.println(word);

            previousLetter = String.valueOf(word.charAt(0));
        }
    }

    /**
     * отримати відсоток голосних літер у даного слова
     */
    public static double getPercentVowel(String word) {
        //лише голосна літера
        Pattern p = Pattern.compile("[eyuioaEYUIOA]");
        Matcher m = p.matcher(word);

        int count = 0;
        while (m.find()) {
            count++;
        }
        return count == 0 ? 0 : (count * 1. / word.length());
    }

    /**
     * Відсортувати слова тексту за зростанням відсотку голосних букв
     * (співвідношення кількості голосних до загальної кількості букв у слові). task 7
     */
    public static void showSortedByVowelPercent(File fName) {
        String text = getTextFromFile(fName);
        getWords(text)
                .stream()
                .sorted(Comparator.comparing(Word::getPercentVowel))
                .forEach(a -> System.out.println(a + "  \t\t = " + (getPercentVowel(a) * 100) + "%"))
        ;
    }

    /**
     * Слова тексту, що починаються з голосних букв, відсортувати в
     * алфавітному порядку по першій приголосній букві слова. task8
     */
    public static void showOnlyVowelSortByConsonant(File fName) {
        String text = getTextFromFile(fName);

        //for vowel words
        Pattern p1 = Pattern.compile(startWithVowel);
        Matcher m1 = p1.matcher(text);

        //for consonant words
        Pattern p2 = Pattern.compile("[^eyuioaEYUIOA]");
        //Matcher m2 = p2.matcher();
        Map<String, String> wordsVowel = new HashMap<>();
        while (m1.find()) {
            //System.out.println(m1.group());
            Matcher m2 = p2.matcher(m1.group());
            if (m2.find()) {
                //System.out.println("consonant: "+m2.group());
                wordsVowel.put(m1.group(), m2.group());
            } else {
                wordsVowel.put(m1.group(), "~");
            }
        }
        System.out.println("Before sorting: ");
        wordsVowel.keySet().forEach(System.out::println);
        System.out.println("\n\n");
        System.out.println("After sorting: ");
        Map<String, String> sorted = wordsVowel.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new
                        ));
        sorted.keySet().forEach(System.out::println);
    }


    /**
     * отримати кількість входжень даного символа
     */
    public static int getAmountOfEntrancesSymbol(String word, String symbol) {
        Pattern p = Pattern.compile(symbol);
        Matcher m = p.matcher(word);

        int count = 0;
        while (m.find()) {
            count++;
        }
        return count;
    }

    public static Map<String, Integer> getSortedByKeyThenByValueToIncrease(Map<String, Integer> map) {
        return map
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k, v) -> v, LinkedHashMap::new))
                ;
    }

    public static Map<String, Integer> getSortedByKeyThenByValueToDecrease(Map<String, Integer> map) {
        return map
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k, v) -> v, LinkedHashMap::new))
                ;
    }

    /**
     * Всі слова тексту відсортувати за зростанням кількості заданої букви у
     * слові. Слова з однаковою кількістю розмістити у алфавітному порядку. task9
     */
    public static void showSortedByCountOfSymbolToIncrease(File fName) {
        Map<String, Integer> map = combined9and13task(fName);
        getSortedByKeyThenByValueToIncrease(map)
                .forEach((k, v) -> System.out.println(k + "=" + v));
    }

    /**
     * Є текст і список слів. Для кожного слова з заданого списку знайти, скільки
     * разів воно зустрічається у кожному реченні, і відсортувати слова за
     * спаданням загальної кількості входжень.  task10
     */
    public static void showSortedByDecreaseOfListWords(File fName) {
        String text = getTextFromFile(fName);
        List<String> list = new ArrayList<>(Arrays.asList("the", "of", "from", "i", "to", "as", "but"));
        List<String> sentences = getSentence(text);
        for (String sentence : sentences) {
            List<String> words = getWords(sentence);
            words.retainAll(list);
            Map<String, Integer> wordsAmount;

            //empty words
            System.out.println("Sentence :" + sentence);
            System.out.println("Words from list: " + words);
            String allWordsFromList = removeBadSym(words.toString() + list.toString());
            wordsAmount = getWordsWithAmount(allWordsFromList);

            //decrease coz i added the list of words, which are not include
            wordsAmount.forEach((k, v) -> wordsAmount.put(k, v - 1));

            System.out.println("Words: " + getSortedByKeyThenByValueToDecrease(wordsAmount) + "\n");
        }
    }

    /**
     * З тексту видалити всі слова заданої довжини, що починаються на
     * приголосну букву.    task12
     */
    public static void showAfterDeletedWordsStartsConsonant(File fName) {
        String text = getTextFromFile(fName);
        int lenDelete = inputInteger();

        //лише приголосні початок (lenDelete -1 coz first symbol is consonant and after n-1)
        Pattern p = Pattern.compile("\\b[\\w&&[^eyuioaEYUIOA]][\\w']{" + (lenDelete - 1) + "}\\b");
        Matcher m = p.matcher(text);

        List<String> wordsAll = getWords(text);
        Set<String> deleteWords = new HashSet<>();
        while (m.find()) {
            //System.out.println("word: "+m.group());
            deleteWords.add(m.group());
        }
        System.out.println("Deleted words: " + deleteWords + "\n");
        wordsAll.removeAll(deleteWords);
        System.out.println("All words: " + wordsAll);
    }

    public static Map<String, Integer> combined9and13task(File fName) {
        String text = getTextFromFile(fName);
        List<String> words = getWords(text);
        Map<String, Integer> map = new HashMap<>();

        System.out.println("Input symbol by which you want sort words");
        String symbol = getInputtedSymbol();

        for (String word : words) {

            int amountOfEntrances = getAmountOfEntrancesSymbol(word, symbol);
            map.put(word, amountOfEntrances);
        }

        System.out.println("Sorted words by " + symbol + ": ");
        return map;
    }

    /**
     * Відсортувати слова у тексті за спаданням кількості входжень заданого
     * символу, а у випадку рівності – за алфавітом. task13
     */
    public static void showSortedByCountOfSymbolToDecrease(File fName) {
        Map<String, Integer> sorted = combined9and13task(fName);
        getSortedByKeyThenByValueToDecrease(sorted)
                .forEach((k, v) -> System.out.println(k + "=" + v));
    }

    /**
     * перевіряє чи є даний рядок(слово) поліндромом
     */
    public static boolean isPalindrome(String word) {
        return word.equals(new StringBuilder(word).reverse().toString());
    }

    /**
     * У заданому тексті знайти підрядок(слова) максимальної довжини, який є
     * паліндромом, тобто, читається зліва на право і справа на ліво однаково.
     * task14
     */
    public static void showAllPalindromes(File fName) {
        String text = getTextFromFile(fName);

        System.out.println("Palindromes:[ ");
        getUniqueWords(text).stream()
                .filter(Word::isPalindrome)
                .forEach(a -> System.out.println(" " + a));
        System.out.println("].");
    }

    /**
     * Перетворити кожне слово у тексті, видаливши з нього всі наступні
     * (попередні) входження першої (останньої) букви цього слова.
     * task15
     **/
    public static void showWordsAfterDeleteAllNextWordsThatStartWithFirstSymbol(File fName) {
        String text = getTextFromFile(fName);
        List<String> words = getWords(text);

        int sizeWords = words.size();

        System.out.println("Words before: " + words);
        for (int i = 0; i < sizeWords; i++) {
            for (int j = i + 1; j < sizeWords; j++) {
                if (words.get(i).charAt(0) == words.get(j).charAt(0)) {
                    words.remove(j);
                    sizeWords--;
                }
            }
        }
        System.out.println("Words after: " + words);
    }

    /**
     * У певному реченні тексту слова заданої довжини замінити вказаним
     * підрядком, довжина якого може не співпадати з довжиною слова.
     * task16
     **/
    public static void showAfterChangeAllWordsInputtedLength(File fName) {
        String text = getTextFromFile(fName);

        int len = inputInteger();
        System.out.println("Input string that you want replace on words inputted length:");
        String newWord = input.nextLine();

        List<String> sentences = getSentence(text);
        System.out.println("Before: ");
        sentences.forEach(System.out::println);

        sentences.replaceAll(a -> a.replaceAll("\\b[\\w']{" + len + "}\\b", newWord));
        System.out.println("\n\nAfter: ");
        sentences.forEach(System.out::println);
    }
}
