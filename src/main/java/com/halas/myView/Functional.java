package com.halas.myView;
import java.io.File;

@FunctionalInterface
public interface Functional {
    void start(File fileName);
}
